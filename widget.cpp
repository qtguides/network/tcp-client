#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    socket(this)
{
    ui->setupUi(this);

    connect(&socket, &QTcpSocket::readyRead, this, &Widget::recibir);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::recibir()
{
    QTextStream stream(&socket);
    ui->mensajes->addItem(stream.readAll());
}

void Widget::on_cerrar_clicked()
{
    close();
}

void Widget::on_conectar_clicked()
{
    uint16_t puerto = static_cast<uint16_t>(ui->puerto->value());
    socket.connectToHost(ui->server->text(), puerto);
    qDebug() << "Conectado al servidor";
}
