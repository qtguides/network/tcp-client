#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
    void recibir();

private slots:
    void on_cerrar_clicked();

    void on_conectar_clicked();

private:
    Ui::Widget *ui;
    QTcpSocket socket;
};

#endif // WIDGET_H
